import './Landing.scss';
import Parallax from 'parallax-js';
import { useEffect } from 'react';

function Landing() {

  const onClickToggleTheme = () => {
    const darkMode = document.querySelector('.dark-mode');
    const body = document.querySelector('body');

    body?.classList.toggle('dark');
    darkMode?.classList.toggle('active');
  }

  useEffect(() => {
    var scene = document.getElementById('scene');
    if (!scene) {
      return;
    }
    var parallaxInstance = new Parallax(scene, {
      relativeInput: true,
    })
  }, [])

  return (
    <div className="landing">
      <div className="header">
        <div className="logo">Summer</div>
        <div className="dark-mode" onClick={onClickToggleTheme}></div>
      </div>
      <div className="main">
        <video autoPlay={true} loop={true} muted={true} src={'./video.mp4'} />
        <div className="txt" id={'scene'}>
          <span data-depth-y="0.2">S</span>
          <span data-depth-y="-0.6">u</span>
          <span data-depth-y="0.5">m</span>
          <span data-depth-y="-0.8">m</span>
          <span data-depth-y="-0.3">e</span>
          <span data-depth-y="0.6">r</span>
        </div>
      </div>
      <div className="footer">
        <div className="down-arrow" />
        <div className="footer-text">Summer 2021</div>
      </div>
    </div>
  );
}

export default Landing;
