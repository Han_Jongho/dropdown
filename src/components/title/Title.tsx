import './Title.scss';
import React from 'react';

type TitleProps = {
  text: string;
}

const Title = ({ text }: TitleProps) => {
  return (
    <div className="title--component">{text}</div> 
  );
}

export default Title;
