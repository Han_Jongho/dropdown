import React from 'react';
import classNames from 'classnames'
import style from './DropdownSubmenu.module.scss';

type SubmenuProps = {
    children: JSX.Element[] | JSX.Element;
    position: 'left' | 'right';
}

const DropdownSubmenu = ({children, position}: SubmenuProps) => (
  <div className={classNames(
    style.submenu,
    position === 'right' && style.submenuRight,
  )}>
    <ul>
      {children}
    </ul>
  </div>
)

DropdownSubmenu.defaultProps = {
  children: null,
  position: 'right',
};

export default DropdownSubmenu;
