import React, {
    useState, useRef, useEffect, useCallback
  } from 'react';
import classNames from 'classnames'
import style from './Dropdown.module.scss';
import Icon from '../icon/Icon';

type DropdownProps = {
    label: string;
    children: JSX.Element[] | JSX.Element;
    isDisabled: boolean;
    position: 'left' | 'right';
}

const Dropdown = ({
    label,
    children,
    isDisabled,
    position,
  }: DropdownProps) => {
    const [isOpen, setOpen] = useState(false);
    const dropdownRef = useRef<HTMLDivElement>(null);
  
    useEffect(() => {
      document.addEventListener('mousedown', handleClick);
  
      return () => {
        document.removeEventListener('mousedown', handleClick);
      };
    }, []);
  
    const handleClick = useCallback((e) => {
      if (!dropdownRef.current?.contains(e.target)) {
        setOpen(false);
      }
    }, []);
  
    const handleButtonOnClick = () => {
      if (isDisabled) {
        return;
      }
  
      setOpen(!isOpen);
    };
  
    return (
      <div
        className={classNames(style.dropdown)}
        ref={dropdownRef}
      >
        <button
          type='button'
          className={classNames(
            style.button,
            isOpen && style.active,
            isDisabled && style.disabled,
          )}
          disabled={isDisabled}
          tabIndex={0}
          onClick={handleButtonOnClick}
        >
          {label}
          <Icon className={"dropdown-down-arrow"} />
        </button>
        {isOpen && (
          <div className={classNames(
            style.menu,
            position === 'right' && style.menuRight,
          )}>
            <ul>
              {children}
            </ul>
          </div>
        )}
      </div>
    );
  };

  Dropdown.defaultProps = {
    children: null,
    isDisabled: false,
    label: 'defualt',
    position: 'right',
  };

export default Dropdown;