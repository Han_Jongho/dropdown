import './Link.scss';
import React, { useCallback } from 'react';

type LinkProps = {
  label: string;
  url: string;
}

const Link = ({ label, url }: LinkProps) => {

  const onClick = useCallback(() => {
    window.open(url);
  }, [url])

  return (
    <div className="link--component" onClick={onClick}>{label}</div> 
  );
}

export default Link;
