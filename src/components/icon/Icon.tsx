import './Icon.scss';
import React from 'react';

type IconProps = {
  className: string;
}

const Icon = ({ className }: IconProps) => {
  return (
    <div className={className}></div> 
  );
}

export default Icon;
