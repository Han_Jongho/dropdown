import './Components.scss';
import React, { useState, useCallback } from 'react';
import Title from './components/title/Title';
import Desc from './components/desc/Desc';
import SimpleDropdown from './simpleDropdown';
import Code from './components/code/Code';
import Separator from './components/separator/Separator';

function Components() {
  const [page, setPage] = useState(0);

  const style = {
    transform: `translateX(${page * -50}%)`,
    transition: `transform 700ms ease-in-out`,
  }

  const onClickPrev = useCallback(() => {
    if (page > 0)
      setPage(page - 1);
  }, [page]);

  const onClickNext = useCallback(() => {
    if (page < 1)
      setPage(page + 1);
  }, [page]);

  return (
    <div className="components">
      <div className="header">Components</div>
      <div className="section">
        {page !== 0 && <div className="prev-btn" onClick={onClickPrev}></div>}
        {page !== 1 && <div className="next-btn" onClick={onClickNext}></div>}
        <div className="contents" style={style}>
          <div className="contents-area">
            <Title text="Dropdown"/>
            <Desc desc="This is a dropdown component" />
            <Separator />
            <SimpleDropdown />
          </div>
          <div className="contents-area">
            <Code>
              {`import Dropdown from './components/dropdown/Dropdown';
import DropdownItem from './components/dropdown/DropdownItem';
import DropdownSubmenu from './components/dropdown/DropdownSubmenu';
import DropdownDivider from './components/dropdown/DropdownDivider';
import { useState } from 'react';

const SimpleDropdown = () => {
  const [currentLabel, setLabel] = useState('Select one');

  const onClick = (label: string) => {
    setLabel(label);
  }

  return (
    <Dropdown label={currentLabel}>
      <DropdownItem label={"Item 1"} onClick={onClick}/>
      <DropdownItem label={"Item 2"} onClick={onClick}>
        <DropdownSubmenu>
          <DropdownItem label={"sub 1"} onClick={onClick}/>
          <DropdownItem label={"sub 2"} onClick={onClick}>
            <DropdownSubmenu>
              <DropdownItem label={"sub sub 1"} onClick={onClick}/>
              <DropdownItem label={"sub sub 2"} onClick={onClick}/>
            </DropdownSubmenu>
          </DropdownItem>
        </DropdownSubmenu>
      </DropdownItem>
      <DropdownItem label={"Item 3"} onClick={onClick}/>
      <DropdownDivider />
      <DropdownItem label={"Item 4"} onClick={onClick}/>
    </Dropdown>
  );
}

export default SimpleDropdown;`}
            </Code>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Components;
